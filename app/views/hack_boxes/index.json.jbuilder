json.array!(@hack_boxes) do |hack_box|
  json.extract! hack_box, :id
  json.url hack_box_url(hack_box, format: :json)
end
