require 'test_helper'

class HackBoxesControllerTest < ActionController::TestCase
  setup do
    @hack_box = hack_boxes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hack_boxes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hack_box" do
    assert_difference('HackBox.count') do
      post :create, hack_box: {  }
    end

    assert_redirected_to hack_box_path(assigns(:hack_box))
  end

  test "should show hack_box" do
    get :show, id: @hack_box
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hack_box
    assert_response :success
  end

  test "should update hack_box" do
    patch :update, id: @hack_box, hack_box: {  }
    assert_redirected_to hack_box_path(assigns(:hack_box))
  end

  test "should destroy hack_box" do
    assert_difference('HackBox.count', -1) do
      delete :destroy, id: @hack_box
    end

    assert_redirected_to hack_boxes_path
  end
end
